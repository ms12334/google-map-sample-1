//Global variables
var map;
var infoWindow;

var locations = [
['Bondi Beach', -33.890542, 151.274856, 4],
['Coogee Beach', -33.923036, 151.259052, 5],
['Cronulla Beach', -34.028249, 151.157507, 3],
['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
['Maroubra Beach', -33.950198, 151.259302, 1]
];

function initialize() {
   var mapOptions = {
      center: new google.maps.LatLng(-33.92, 151.25),
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
   };
   map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

   infoWindow = new google.maps.InfoWindow();

   google.maps.event.addListener(map, 'click', function() {
      infoWindow.close();
   });

   displayMarkers();      
}
google.maps.event.addDomListener(window, 'load', initialize);

function displayMarkers(){
    var bounds = new google.maps.LatLngBounds();
    
	for (var i = 0; i < locations.length; i++){

        var latLng = new google.maps.LatLng(locations[i][1], locations[i][2]);
        var content = locations[i][0];

        createMarker(latLng, content);
        bounds.extend(latLng);  
    }//end of for loop
	
	map.fitBounds(bounds);
}

function createMarker(latLng, content){
    var marker = new google.maps.Marker({
      map: map,
      position: latLng,
	  title: content
    });

    google.maps.event.addListener(marker, 'click', function() {
        var iwContent = content;
        infoWindow.setContent(iwContent);
        infoWindow.open(map, marker);
   });
}